from django.shortcuts import render, redirect
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.models import User, auth
from django.http import HttpResponse
from django.contrib import messages

def landing(request):
	return render(request, 'landing.html') 

def home(request):
	return render(request, 'home.html')

def signup(request):
	if request.method == 'POST':
		username = request.POST['username']
		password= request.POST['password']

		if username and password:
			if User.objects.filter(username=username).exists():
				messages.info(request, "Username already taken")
				return redirect('/')

			else:
				user = User.objects.create_user(username=username, password=password)
				user.save()
				user = auth.authenticate(username=username, password=password)
				login(request, user)
				return redirect('/home/')
		else:
			messages.info(request, "Please fill the form first.")
			redirect('/')

	else:
		return render(request, '/')


def logn(request):
	if request.method == 'POST':
		username = request.POST['username']
		password = request.POST['password']

		user = authenticate(username=username, password=password)

		if user is not None:
			login(request, user)
			return redirect('/home/')
		else:
			messages.info(request, 'Invalid login info')
			return redirect('/')

def logut(request):
	logout(request)
	request.session.flush()
	redirect('/')
