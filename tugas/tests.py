import unittest

from django.test import TestCase, Client
from django.urls import resolve

from selenium import webdriver
from selenium.webdriver.firefox.options import Options

from .views import *

import time

class UnitTest(TestCase):

	def test_url_exists(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)
	def test_url_that_does_not_exist(self):
		response = Client().get('/nothing/')
		self.assertEqual(response.status_code, 404)

	def test_use_form_template(self):
		response = Client().get('/')
		self.assertTemplateUsed(response, 'landing.html')

	def test_signup_is_inside_landing(self):
		response = Client().get('/')
		content = response.content.decode('utf8')
		self.assertIn('Welcome', content)
		self.assertIn('Login', content)
		self.assertIn('Sign up', content)
	def test_form_is_inside_landing(self):
		response = Client().get('/')
		content = response.content.decode('utf8')
		self.assertIn('<form', content)

	def test_halo_is_inside_home(self):
		response = Client().get('/home/')
		content = response.content.decode('utf8')
		self.assertIn('Halo', content)
	def test_button_is_inside_landing(self):
		response = Client().get('/home/')
		content = response.content.decode('utf8')
		self.assertIn('<button', content)
		self.assertIn('Logout', content)

	def test_page_uses_views_function(self):
		found = resolve('/')
		self.assertEqual(found.func, landing) #Views's function

class SeleniumFunctionalTest(TestCase):

	def setUp(self):
		options = Options()
		options.add_argument('--headless')
		self.selenium = webdriver.Firefox(firefox_options=options)
		super(SeleniumFunctionalTest, self).setUp()

	def tearDown(self):
		self.selenium.quit()
		super(SeleniumFunctionalTest,self).tearDown()

	def test_inserting_data_to_form(self):
		#Inserting one data
		self.selenium.get('http://127.0.0.1:8000/')
		time.sleep(10)

		signin_name = self.selenium.find_element_by_id("id_signin_user")
		signin_name.send_keys("name")
		signin_pass = self.selenium.find_element_by_id("id_signin_pass")
		signin_pass.send_keys("pass")
		signin = self.selenium.find_element_by_id("button_signin")
		signin.click()
		time.sleep(10)
		self.assertIn("Welcome", self.selenium.page_source)

		login_name = self.selenium.find_element_by_id("id_login_user")
		login_name.send_keys("name")
		login_pass = self.selenium.find_element_by_id("id_login_pass")
		login_pass.send_keys("pass")
		login = self.selenium.find_element_by_id("button_login")
		login.click()
		time.sleep(10)

		self.selenium.get('http://127.0.0.1:8000/home')

		self.assertIn("Halo", self.selenium.page_source)
		self.assertIn("Logout", self.selenium.page_source)

		logout = self.selenium.find_element_by_id("button_logout")
		logout.click()
		time.sleep(10)

		self.assertIn("Welcome", self.selenium.page_source)