function search_books(query){
    event.preventDefault();
    var search = document.getElementById('query-search').value
    console.log("WORKS");

    if(query != "Troops"){
        query = search;
    }

     $.ajax({
        type:"GET",
        url:"/get_json/",
        data:{
            'query': query
        },
        dataType: 'json',
        success: function(json){
            $('#search-content').empty();
            $('#search-content').append('<tr><th scope = "col">Cover</th><th scope = "col">Judul</th><th scope = "col">Deskripsi</th><th scope = "col">Authors</th></tr>')

            var books = json.items;
            for(i=0; i < books.length; i++) {

                var cover = books[i].volumeInfo.imageLinks.thumbnail;
                if(cover == undefined){
                    cover = "No image found";
                } else {
                    cover = "<img src=" + cover + ">";
                }

                var title = books[i].volumeInfo.title;
                if(title == undefined){
                    title = "No title";
                }

                var desc = books[i].volumeInfo.description;
                if(desc == undefined){
                    desc = "No description available";
                }

                // Saya memanggil author yang pertama saja karena saya tidak mengerti
                // Bagaimana looping yang baik untuk mengambil seluruh author
                // Walaupun sebenarnya bisa dimasukkan author-author tersebut
                // Ke dalam sebuah unordered list

                var author = books[i].volumeInfo.authors;
                if(author == undefined){
                    author = "No authors found";
                } else {
                    author = author[0];
                }

                $('#search-content').append('<tr><th>' + cover + '</th><th>' + title + '</th><th><p>' + desc + '</p></th><th>' + author + '</th></tr>');
            }
        }
    });

    return false;
}

$(document).on("submit", "#search-form", function(event){
    event.preventDefault();
    var query = $('#query-form').val();
    search_books(query);
});

$(document).ready(function(){
    var query = "Troops";
    search_books(query);
});